import os
import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()

token = os.getenv('DISCORD_TOKEN')
server = os.getenv('SERVER')
bot = commands.Bot(command_prefix='!')

@bot.event
async def on_ready():
    print(f'{bot.user} has connected to Discord!')
    guild = discord.utils.get(bot.guilds, name=server)

    print(
        f'{bot.user} is connected to:\n'
        f'{guild.name} (id: {guild.id})'
    )

@bot.command(name='addsong', help='Adds songs to download list (use quotes): !addsong "Sweet Emotion" "Aerosmith"')
async def add_song(ctx, song, artist):
    print(f'received request to add {song} by {artist}')
    response = f'added {song} by {artist} to the download list'
    pins = await ctx.message.channel.pins()
    print(pins)
    (_, song_list) = lambda p: p.content.startswith('Song List'), pins
    messages = [m for m in song_list if m.author == bot.user]
    message = messages[0]

    print(message)
    await message.edit(content=f'{message.content}\n{song} - {artist}')
    print(message)
    await ctx.send(response)


@bot.command(name='init-list')
async def init_list(ctx):
    message = await ctx.message.channel.send('Song List:\n')
    await message.pin()
    await ctx.send('list initialized')

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    
    response = None
    
    if message.channel.name == 'bot-spam':
        if 'what' in message.content.lower():
            response = 'Say what again muthafukka'
            await message.channel.send(response)
    
    if 'home' in message.content.lower():
        response = 'holme*'
    
    if 'both' in message.content.lower():
        if response:
            response = f'{response} bolth*'
        else:
            response = 'bolth*'
    
    if response:
        await message.channel.send(response)
    
    await bot.process_commands(message)

bot.run(token)
